package deloitte.academy.controller;

/**
 * Esta clase tiene 5 diferentes metodos 3 con operadores ternario, 1 con una
 * condici�n if y la �ltima con elseif.)
 * 
 * @author Emiliano Oliveros
 * @since 11/03/2020
 *
 */
public class Condicionales {

	/**
	 * Este metodo evalua si el valor insertado es mayor a 0 a traves de un operador
	 * ternario.
	 * 
	 * @param int n
	 * @return boolean result
	 */
	public static boolean condicional1(int n) {

		boolean result = n > 0 ? true : false;
		return result;
	}

	/**
	 * Este metodo evalua si el valor insertado es menor a 0 a traves de un operador
	 * ternario.
	 * 
	 * @param int n
	 * @return boolean result
	 */
	public static boolean condicional2(int n) {

		boolean result = n < 0 ? true : false;
		return result;
	}

	/**
	 * Este metodo evalua si el valor insertado es igual a 0 a traves de un operador
	 * ternario.
	 * 
	 * @param int n
	 * @return boolean result
	 */

	public static boolean condicional3(int n) {

		boolean result = n == 0 ? true : false;
		return result;
	}

	/**
	 * Este metodo evalua si el primer numero es mayor.
	 * 
	 * @param int i
	 * @param int j
	 * @return int numeroMayor
	 */
	public static int condicional4(int i, int j) {

		int numeroMayor = 0;
		if (i > j) {
			numeroMayor = i;
		}
		return numeroMayor;
	}

	/**
	 * Este metodo compara dos numero y nos regresa el numero mayor.
	 * 
	 * @param int i
	 * @param int j
	 * @return int numero mayor
	 */
	public static int condicional5(int i, int j) {

		int numeroMayor = 0;
		if (i > j) {
			numeroMayor = i;
		} else {
			numeroMayor = j;
		}
		return numeroMayor;

	}

}
