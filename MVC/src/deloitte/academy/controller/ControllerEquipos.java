package deloitte.academy.controller;

import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import deloitte.academy.model.Equipos;
import deloitte.academy.run.Main;

public class ControllerEquipos {
	private static final Logger LOGGER = Logger.getLogger(Logger.class.getName());

	Equipos equipoPrueba = new Equipos();

	/**
	 * Metodo que agrega un objeto equipo a una lista
	 * 
	 * @param Objeto de la clase Equipo
	 * @exception Mensaje de ERROR
	 */
	public void Agregar(Equipos equipo) {
		try {
			Main.listaEquipos.add(equipo);
			LOGGER.info("Equipo agregado");
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "ERROR", ex);
		}
	}

	/**
	 * M�todo en el cual se declaran par�metros de los objetos y posteriormente los
	 * a la lista de Equipos
	 * 
	 * @param nombreEquipo     String
	 * @param due�oEquipo      String
	 * @param ciudadEquipo     String
	 * @param titulosEquipo    String
	 * @param antiguedadEquipo String
	 * @param atributo1        String
	 * @param atributo2        String
	 * @param atributo3        String
	 * @param atributo4        String
	 * @param atributo5        String
	 * @param atributo6        String
	 * @param atributo7        String
	 * @param atributo8        String
	 * @param atributo9        String
	 * @param atributo10       String
	 * @param atributo11       String
	 * @param atributo12       String
	 * @param atributo13       String
	 * @param atributo14       String
	 * @param atributo15       String
	 */

	public void agregarPorAtributos(String nombreEquipo, String due�oEquipo, String ciudadEquipo, int titulosEquipo,
			int antiguedadEquipo, int atributo1, int atributo2, int atributo3, int atributo4,
			int atributo5, int atributo6, int atributo7, int atributo8, int atributo9, int atributo10,
			int atributo11, int atributo12, int atributo13, int atributo14, int atributo15) {
		try {
			equipoPrueba.setNombreEquipo(nombreEquipo);
			equipoPrueba.setDue�oEquipo(due�oEquipo);
			equipoPrueba.setCiudadEquipo(ciudadEquipo);
			equipoPrueba.setTitulosEquipo(titulosEquipo);
			equipoPrueba.setAntiguedadEquipo(antiguedadEquipo);
			equipoPrueba.setAtributo1(atributo1);
			equipoPrueba.setAtributo2(atributo2);
			equipoPrueba.setAtributo3(atributo3);
			equipoPrueba.setAtributo4(atributo4);
			equipoPrueba.setAtributo5(atributo5);
			equipoPrueba.setAtributo6(atributo6);
			equipoPrueba.setAtributo7(atributo7);
			equipoPrueba.setAtributo8(atributo8);
			equipoPrueba.setAtributo9(atributo9);
			equipoPrueba.setAtributo10(atributo10);
			equipoPrueba.setAtributo11(atributo11);
			equipoPrueba.setAtributo12(atributo12);
			equipoPrueba.setAtributo13(atributo13);
			equipoPrueba.setAtributo14(atributo14);
			equipoPrueba.setAtributo15(atributo15);
			Main.listaEquipos.add(equipoPrueba);
			LOGGER.info("Equipo agregado");

		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "ERROR", ex);
		}
	}

	/**
	 * Metodo que busca un equipo en la lista de equipos
	 * 
	 * @param nombreEquipo tipo String
	 * @return un objeto equipoPruba si encontro dicho equipo en la lista
	 */

	public Equipos buscarPorNombre(String nombreEquipo) {
		Main.listaEquipos.forEach(new Consumer<Equipos>() {

			@Override
			public void accept(Equipos equipo) {
				if (equipo.getNombreEquipo() == nombreEquipo) {
					equipoPrueba = equipo;
				}
			}
		});
		return equipoPrueba;
	}

	/**
	 * Metodo que imprime el nombre del equipo y due�o del mismo
	 * 
	 * @see no recibe par�metos y no regresa nada
	 * 
	 */

	public void buscarTodos() {
		Main.listaEquipos.forEach(new Consumer<Equipos>() {

			@Override
			public void accept(Equipos equipo) {
				System.out.println(
						"Nombre Equipo: " + equipo.getNombreEquipo() + "\n Due�o Equipo: " + equipo.getDue�oEquipo());

			}

		});
	}

}
