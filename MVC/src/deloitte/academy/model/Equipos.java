package deloitte.academy.model;

/**
 * Clase entidad Equipos que contiene 20 atributos.
 * 
 * @author Emiliano Oliveros
 * @since 11/03/2020
 */
public class Equipos {
	/*
	 * Declaracion de variables.
	 */
	private String nombreEquipo;
	private String due�oEquipo;
	private String ciudadEquipo;
	private int titulosEquipo;
	private int antiguedadEquipo;
	private int atributo1;
	private int atributo2;
	private int atributo3;
	private int atributo4;
	private int atributo5;
	private int atributo6;
	private int atributo7;
	private int atributo8;
	private int atributo9;
	private int atributo10;
	private int atributo11;
	private int atributo12;
	private int atributo13;
	private int atributo14;
	private int atributo15;

	/*
	 * Constructores
	 */
	public Equipos() {
		// TODO Auto-generated constructor stub
	}

	public Equipos(String nombreEquipo, String due�oEquipo, String ciudadEquipo, int titulosEquipo,
			int antiguedadEquipo, int atributo1, int atributo2, int atributo3, int atributo4, int atributo5,
			int atributo6, int atributo7, int atributo8, int atributo9, int atributo10, int atributo11, int atributo12,
			int atributo13, int atributo14, int atributo15) {
		super();
		this.nombreEquipo = nombreEquipo;
		this.due�oEquipo = due�oEquipo;
		this.ciudadEquipo = ciudadEquipo;
		this.titulosEquipo = titulosEquipo;
		this.antiguedadEquipo = antiguedadEquipo;
		this.atributo1 = atributo1;
		this.atributo2 = atributo2;
		this.atributo3 = atributo3;
		this.atributo4 = atributo4;
		this.atributo5 = atributo5;
		this.atributo6 = atributo6;
		this.atributo7 = atributo7;
		this.atributo8 = atributo8;
		this.atributo9 = atributo9;
		this.atributo10 = atributo10;
		this.atributo11 = atributo11;
		this.atributo12 = atributo12;
		this.atributo13 = atributo13;
		this.atributo14 = atributo14;
		this.atributo15 = atributo15;
	}

	public String getNombreEquipo() {
		return nombreEquipo;
	}

	public void setNombreEquipo(String nombreEquipo) {
		this.nombreEquipo = nombreEquipo;
	}

	public String getDue�oEquipo() {
		return due�oEquipo;
	}

	public void setDue�oEquipo(String due�oEquipo) {
		this.due�oEquipo = due�oEquipo;
	}

	public String getCiudadEquipo() {
		return ciudadEquipo;
	}

	public void setCiudadEquipo(String ciudadEquipo) {
		this.ciudadEquipo = ciudadEquipo;
	}

	public int getTitulosEquipo() {
		return titulosEquipo;
	}

	public void setTitulosEquipo(int titulosEquipo) {
		this.titulosEquipo = titulosEquipo;
	}

	public int getAntiguedadEquipo() {
		return antiguedadEquipo;
	}

	public void setAntiguedadEquipo(int antiguedadEquipo) {
		this.antiguedadEquipo = antiguedadEquipo;
	}

	public int getAtributo1() {
		return atributo1;
	}

	public void setAtributo1(int atributo1) {
		this.atributo1 = atributo1;
	}

	public int getAtributo2() {
		return atributo2;
	}

	public void setAtributo2(int atributo2) {
		this.atributo2 = atributo2;
	}

	public int getAtributo3() {
		return atributo3;
	}

	public void setAtributo3(int atributo3) {
		this.atributo3 = atributo3;
	}

	public int getAtributo4() {
		return atributo4;
	}

	public void setAtributo4(int atributo4) {
		this.atributo4 = atributo4;
	}

	public int getAtributo5() {
		return atributo5;
	}

	public void setAtributo5(int atributo5) {
		this.atributo5 = atributo5;
	}

	public int getAtributo6() {
		return atributo6;
	}

	public void setAtributo6(int atributo6) {
		this.atributo6 = atributo6;
	}

	public int getAtributo7() {
		return atributo7;
	}

	public void setAtributo7(int atributo7) {
		this.atributo7 = atributo7;
	}

	public int getAtributo8() {
		return atributo8;
	}

	public void setAtributo8(int atributo8) {
		this.atributo8 = atributo8;
	}

	public int getAtributo9() {
		return atributo9;
	}

	public void setAtributo9(int atributo9) {
		this.atributo9 = atributo9;
	}

	public int getAtributo10() {
		return atributo10;
	}

	public void setAtributo10(int atributo10) {
		this.atributo10 = atributo10;
	}

	public int getAtributo11() {
		return atributo11;
	}

	public void setAtributo11(int atributo11) {
		this.atributo11 = atributo11;
	}

	public int getAtributo12() {
		return atributo12;
	}

	public void setAtributo12(int atributo12) {
		this.atributo12 = atributo12;
	}

	public int getAtributo13() {
		return atributo13;
	}

	public void setAtributo13(int atributo13) {
		this.atributo13 = atributo13;
	}

	public int getAtributo14() {
		return atributo14;
	}

	public void setAtributo14(int atributo14) {
		this.atributo14 = atributo14;
	}

	public int getAtributo15() {
		return atributo15;
	}

	public void setAtributo15(int atributo15) {
		this.atributo15 = atributo15;
	}

	



	






}
