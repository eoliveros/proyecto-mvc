package deloitte.academy.run;

import java.util.ArrayList;
import java.util.List;

import deloitte.academy.controller.ControllerEquipos;
import deloitte.academy.model.Equipos;


/**
 * Clase que ejecuta el proyecto.
 * 
 * @author Emiliano Oliveros
 * @since 11/03/2020
 */
public class Main {
	public static final List<Equipos> listaEquipos = new ArrayList<Equipos>();

	public static void main(String[] args) {
		Equipos equipoImp = new Equipos();
		Equipos equipo1 = new Equipos("Cruz Azul", "Cementos Cruz Azul", 
				"Cd Mexico", 8, 100, 1, 2, 
				3, 4, 5, 6, 7, 8, 9,
				10, 11, 12, 13, 14, 15);
		
		ControllerEquipos dP = new ControllerEquipos();
		dP.Agregar(equipo1);

		System.out.println("BUSCAR POR NOMBRE: ");
		equipoImp = dP.buscarPorNombre("Cruz Azul");

		System.out.println("Nombre Equipo: " + equipoImp.getNombreEquipo() + "\n Due�o Equipo: "
				+ equipoImp.getDue�oEquipo() + "\n Ciudad Equipo: " + equipoImp.getCiudadEquipo() + "\nTitulos Equipo: "
				+ equipoImp.getTitulosEquipo() + "\n Antiguedad Equipo: " + equipoImp.getAntiguedadEquipo()
				+ "\n Atributo1: " + equipoImp.getAtributo1());

		System.out.println("BUSCAR TODOS");
		dP.buscarTodos();
	}
}
